// stack.h by D.Kitajima*****

//void initStack(stack *sp, int size_of_stack); // スタックを初期化する関数
//double topValue(stack *sp); // スタックの一番上の数値を表示する関数
//void  push(stack *sp, double x); // スタックに push する関数
//double  pull(stack *sp); // スタックから pull する関数のプロトタイプ

#define N 10000
typedef struct {
    double data[N];
    int top;
} stack;
void initStack(stack *sp, int n);  // スタックを初期化する関数
double topValue(stack *sp); // スタックの一番上の数値を表示する関数
void push(stack *sp, double x); // スタックに push する関数
double pull(stack *sp); // スタックから pull する関数のプロトタイプ
void  printStack(stack *sp); // スタックの内容を一覧表示する関数
void addStack(stack *sp); // スタックから二つの値を取り出し，和をスタックに積む関数
void mulStack(stack *sp); // 同じく，積をスタックに積む関数
void subStack(stack *sp); // 同じく，差をスタックに積む関数 (順番に注意)
void divStack(stack *sp); // 同じく，商をスタックに積む関数 (順番に注意)
void sinStack(stack *sp); // sinの値を返す
void cosStack(stack *sp); // cosの値を返す
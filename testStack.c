// testStack.c by D.Kitajima*****

#include <stdio.h>
#include <math.h>
#include "stack.h"
#include "testCommon.h"

void testInitStack() {
    stack myStack;
    testStart("initStack");
    myStack.top = 0; // 適当な値にセット
    initStack(&myStack, N);
    assert_equals_int(myStack.top, N); // sp が最終値より後ろになっているか
    testEnd();
}

void testTopValue(void) {
    stack myStack;
    testStart("topValue");
    initStack(&myStack, N);
    assert_equals_int(isnan(topValue(&myStack)), 1);
    myStack.top = 0;
    myStack.data[0] = 5.0;
    assert_equals_int(isnan(topValue(&myStack)), 0);
    assert_equals_double(topValue(&myStack), 5.0);
    testEnd();
}

void testPush() {
    stack myStack;
    testStart("push");
    initStack(&myStack, N);
    push(&myStack, 2.0);
    assert_equals_double(topValue(&myStack), 2.0);
    push(&myStack, N-1);
    assert_equals_double(topValue(&myStack), N-1);
    testEnd();
    
}

void testPull() {
    stack myStack;
    testStart("pull");
    initStack(&myStack, N);
    push(&myStack, 2.5);
    push(&myStack, 5.0);
    assert_equals_double(pull(&myStack), 5.0);
    assert_equals_int(myStack.top, N-1);
    assert_equals_double(pull(&myStack), 2.5);
    assert_equals_int(myStack.top, N);
    assert_equals_int(isnan(pull(&myStack)), 1);
    testEnd();

}

void testAddStack() {
    stack myStack;
    testStart("addStack");
    initStack(&myStack, N);
    push(&myStack, 2.0);
    push(&myStack, 3.0);
    addStack(&myStack);
    assert_equals_double(topValue(&myStack), 5.0);
    assert_equals_int(myStack.top, N-1);
    addStack(&myStack);
    assert_equals_int(isnan(topValue(&myStack)), 1);
    assert_equals_int(myStack.top, N-1);
    testEnd();
}

void testMulStack() {
    stack myStack;
    testStart("mulStack");
    initStack(&myStack, N);
    push(&myStack, 2.0);
    push(&myStack, 3.0);
    mulStack(&myStack);
    assert_equals_double(topValue(&myStack), 6.0);
    assert_equals_int(myStack.top, N-1);
    mulStack(&myStack);
    assert_equals_int(isnan(topValue(&myStack)), 1);
    assert_equals_int(myStack.top, N-1);
    testEnd();
}

void testSubStack() {
    stack myStack;
    testStart("subStack");
    initStack(&myStack, N);
    push(&myStack, 3.0);
    push(&myStack, 2.0);
    subStack(&myStack);
    assert_equals_double(topValue(&myStack), 1.0);
    assert_equals_int(myStack.top, N-1);
    subStack(&myStack);
    assert_equals_int(isnan(topValue(&myStack)), 1);
    assert_equals_int(myStack.top, N-1);
    testEnd();
}

void testDivStack() {
    stack myStack;
    testStart("divStack");
    initStack(&myStack, N);
    push(&myStack, 4.0);
    push(&myStack, 2.0);
    divStack(&myStack);
    assert_equals_double(topValue(&myStack), 2.0);
    assert_equals_int(myStack.top, N-1);
    divStack(&myStack);
    assert_equals_int(isnan(topValue(&myStack)), 1);
    assert_equals_int(myStack.top, N-1);
    testEnd();
}

void testPrintStack() {
    stack myStack;
    testStart("printStack");
    initStack(&myStack, N);
    push(&myStack, 5.0);
    push(&myStack, 3.0);
    printStack(&myStack);
    testEnd();
}

void testSinStack() {
    stack myStack;
    testStart("sinStack");
    initStack(&myStack, N);
    push(&myStack, 0.5*M_PI);
    sinStack(&myStack);
    assert_equals_double(topValue(&myStack), 1.0);
    assert_equals_int(myStack.top, N-1);
    testEnd();
}

void testCosStack() {
    stack myStack;
    testStart("cosStack");
    initStack(&myStack, N);
    push(&myStack, M_PI);
    cosStack(&myStack);
    assert_equals_double(topValue(&myStack), -1.0);
    assert_equals_int(myStack.top, N-1);
    testEnd();
}

int main() {
    testInitStack();
    testTopValue();
    testPush();
    testPull();
    testAddStack();
    testMulStack();
    testSubStack();
    testDivStack();
    testPrintStack();
    testSinStack();
    testCosStack();
	return 0;
}

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "stack.h"
#include <string.h>
#define M 200

char line[M];

int main() {
    stack myStack;
    initStack(&myStack, N);
    double data;
    while (1) {
        fprintf(stderr, "Hello World!->");
        fgets(line, M, stdin);
        if (strcmp("+\n", line) == 0) {
            addStack(&myStack);
        } else if (strcmp("-\n", line) == 0) {
            subStack(&myStack);
        } else if (strcmp("*\n", line) == 0) {
            mulStack(&myStack);
        } else if (strcmp("/\n", line) == 0) {
            divStack(&myStack);
        } else if (strcmp("p\n", line) == 0) {
            printf("%g \n", pull(&myStack));
        } else if (strcmp("clear\n", line) == 0) {
            initStack(&myStack, N);
            printf("MyStack was cleared.\n");
        } else if (strcmp("sin()\n", line) == 0) {
            sinStack(&myStack);
        } else if (strcmp("cos()\n", line) == 0) {
            cosStack(&myStack);
        } else if (strcmp("e\n", line) == 0) {
            break;
        } else {
            data = atof(line);
            if (data != 0) {
                push(&myStack, data);
            }
        }
        printStack(&myStack);
    }

    return 0;
}

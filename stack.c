// stack.c by 4318 D.Kitajima ******
#include "stack.h"
#include <stdio.h>
#include <math.h>
void initStack(stack *sp, int n) {
    sp->top = n;
}

double topValue(stack *sp) {
    if (sp->top == N) {
        return sqrt(-1.0);
    } else {
        return sp->data[sp->top];
    }
}

void push(stack *sp, double x) {
    sp->top -= 1.0;
    sp->data[sp->top] = x;
}

double pull(stack *sp) {
    double temp;
    if (sp->top == N) {
        return sqrt(-1.0);
    } else {
        temp = sp->data[sp->top];
        sp->top += 1.0;
        return temp;
    }
}

void addStack(stack *sp) {
    push(sp, pull(sp) + pull(sp));
}

void mulStack(stack *sp) {
    push(sp, pull(sp) * pull(sp));
}

void subStack(stack *sp) {
    double temp;
    temp = pull(sp);
    push(sp, pull(sp)-temp);
}

void divStack(stack *sp) {
    double temp;
    temp = pull(sp);
    push(sp, pull(sp) / temp);
}

void printStack(stack *sp) {
    int i;
    for (i = N; i > sp->top; i--) {
        printf("%g\n", sp->data[i-1]);
        if(sp->top == i-1) {
            printf("________________\n");
        }    
    }
}

void sinStack(stack *sp) {
    push(sp, sin(pull(sp)));
}

void cosStack(stack *sp) {
    push(sp, cos(pull(sp)));
}
